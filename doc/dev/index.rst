Welcome to Abelujo's developer documentation
============================================

Contents:

.. toctree::
   :maxdepth: 2

   choices.rst
   angular-crash-course
   abelujo-dev.rst
   webscraping.rst
   django-dev.rst
   clientside-dev.rst
   editors.rst


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
