# Docker

*warning: Dockerfile not ready*

One of the goals of Docker is to build images with all dependencies
installed (system and application wide) which are ready to be
deployed.

It can also be used to fully test the application. Thus, we never
forget a dependency.

https://www.docker.com/whatisdocker/
